// loading the expressjs module into our app and into the express variable
// express module will allow us to use expressjs methods to create our API.
const express = require("express");

// Creates an application with expressjs.
// This creates an expressjs application and stores it as app.
// app is our server.
const app = express();

// port is a variable to contain the port when we want to designate.
const port = 4000;

// express.json() allows us to handle the request's body and automatically parse the incoming JSON to a JS object we can access and manage.
app.use(express.json());

let users = [

	{
		email: "mighty12@gmail.com",
		username: "mightyMouse12",
		password: "noreltatedtomickey",
		isAdmin: false
	},
	{
		email: "minnieMouse@gmail.com",
		username: "minniemickey",
		password: "minniesincethestart",
		isAdmin: false
	},
	{
		email: "mickeyTheMouse@gmail.com",
		username: "mickeyKing",
		password: "thefacethatrunstheplace",
		isAdmin: true
	}

];

let items =[];

let loggedUser = {};

// Express has methods to use as routes corresponding to each HTTP method.
// get(<endpoint>,<functionToHandle request and responses>)
app.get('/',(req,res)=>{

	// once the route is accessed, we can send a response with the use of res.send
	//res.end() actually combines writeHead() and end() already.
	// It is used to send a response to the clente and is the end of the response.
	res.send("Hello World!");

})

app.get('/hello',(req,res)=>{
	res.send("Hello from Batch 123!");
});

app.post('/',(req,res)=>{
	// req.body will contain the  body of a request.
	console.log(req.body);
	// Mini-Activity
	// Change the message that we are send into a string:
	// "Hello, I'm <nameFromReqest>.I am <ageFromRequest>. I could be described as <descriptionFromRequests>."
	res.send(`Hello, I'm ${req.body.name}. I am ${req.body.age} I could be described as ${req.body.description}`);
});

//register

app.post('/users',(req,res)=>{

	// since 2 application are communicating with one another, being our client and our API, it is a good practice to always consolelog the incoming data first.
	console.log(req.body);

	//simulate creation of new user document
	let  newUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	}

	users.push(newUser);
	console.log(users);
	res.send("Registered Successfully.")
});

//login
app.post('/users/login',(req,res)=>{

// should contain  username and  password
console.log(req.body)
	// find the user with the same username and password from our request body.
	let foundUser =  users.find((user)=>{
		return user.username === req.body.username &&  user.password === req.body.password;
	});

	if(foundUser !== undefined){
		//Temporarily log  our user in. Allows us to refer the details of a logged in user.
		loggedUser =  foundUser;
		console.log(loggedUser);

		res.send("Thank you for logging in.")
	} else {
		console.log("Incorrect username or password")
		res.send("Login failed. Wrong Credentials")
	}
})

// addItem
app.post('/items',(req,res)=>{
	console.log(loggedUser);
	let  addedItems = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive
	}
	if (loggedUser.isAdmin === true){
		items.push(addedItems)
		console.log(items)
		res.send("You have added a new item")//test
	} else {
		res.send("Unauthorized: Action Forbidden.")
	}
})

app.get('/items',(req,res)=>{
console.log(loggedUser)

if (loggedUser.isAdmin === true){
		res.send(items)
	} else {
		res.send("Unauthorized: Action Forbidden.")
	}

})

app.listen(port,() => console.log(`Server is running  at port ${port}`));